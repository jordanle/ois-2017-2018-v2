window.addEventListener('load', function() {
	//stran nalozena
	
	
	var prijava = function() {
		var ime = document.getElementById('uporabnisko_ime').value;
		document.getElementById('uporabnik').innerHTML = ime;
		
		document.querySelector('.pokrivalo').style.visibility = 'hidden';
	}
	
	var gumb = document.getElementById('prijavniGumb');
	gumb.addEventListener('click', prijava);
	
	

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			
			
			if(cas === 0) {
				alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector('.naziv_opomnika').innerHTML + " je potekla!");
				
				opomnik.parentNode.removeChild(opomnik);
				
				
			}
			else {
				cas -= 1;
				casovnik.innerHTML = cas;
			}
			
			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

	var dodajOpomnik = function() {
		var naziv = document.getElementById('naziv_opomnika').value;
		var cas = document.getElementById('cas_opomnika').value; 
		
		document.getElementById('naziv_opomnika').value = "";
		document.getElementById('cas_opomnika').value = "";
		
		var opomnik = 
		"<div class='opomnik senca rob'>" 
			+ "<div class='naziv_opomnika'>" + naziv + "</div>"
			+ "<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div>"
		+ "</div>";
		
		document.getElementById('opomniki').innerHTML += opomnik;
	}
	
	document.getElementById('dodajGumb').addEventListener('click', dodajOpomnik);

});

